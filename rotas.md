### Rotas da API

rotas /

    User:

        get /users/ // listar todos os usuários
        get /users/:userId // lista apenas um usuário
        post /users/ // criar um usuário
        patch /users/:userId // atualizar um usuário
        delete /users/:userId // deletar um usuário

    Group:

        get /groups/ listar todos os grupos
        get /groups/:groupId // viazualizar apenas um grupo
        post /groups/ // criar um grupo
        patch /groups/:groupId // atualizar um grupo
        delete /groups/:groupId // deletar um grupo

    Group Members:
        get /groups/:groupId/members/   //  listar membros do grupo
        post /groups/:groupId/members/memberId  //  adicionar membro no grupo
        delete /groups/:groupId/members/memberId    //  remover membro do grupo

    Group Categories:
        get /groups/:groupId/categories     //listar todas as categorias de um grupo
        get /groups/:groupId/categories/:categoryId     // ver apenas uma categoria de um grupo
        post    /groups/:groupId/categories     //  criar uma categoria de um grupo
        patch   /groups/:groupId/categories/:categoryId     // atualizar uma  categoria de um grupo
        delete  /groups/:groupId/categories/:categoryId     //  deletar uma categoria de um grupo

    Category Tasks:
        get /categories/:categoryId/groups/:groupId/tasks/         //  Listar todas as tarefas da categoria
        get /categories/:categoryId/groups/:groupId/tasks/:taskId          //  Ver uma tarefa especifica da categoria
        post /categories/:categoryId/groups/:groupId/tasks/            //  Criar uma tarefa na categoria
        patch /categories/:categoryId/groups/:groupId/tasks/:taskId            //  Atualizar uma tarefa na categoria
        delete /categories/:categoryId/groups/:groupId/tasks/:taskId           //  deletar uma tarefa na categoria
